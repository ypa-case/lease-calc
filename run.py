import pandas as pd
df = pd.read_excel('wit_mnd_nl_std_alg_20210101.xlsx', engine='openpyxl', usecols="A")
df2 = pd.read_excel('wit_mnd_nl_std_alg_20210101.xlsx', engine='openpyxl', usecols="C")
mylist = df['Tabelloon'].tolist()
mylist.pop(-1)
var = df2.iat[559-2, 0]
print(var)

brutoloon = 2500
diff_list = []

for i in mylist:
    if type(i) == int:
        j = float(i)
        diff = brutoloon - j
        diff_list.append(diff)
    elif type(i) == float:
        diff = brutoloon - i
        diff_list.append(diff)

lowest = diff_list[min(range(len(diff_list)), key = lambda i: abs(diff_list[i]-0))]
index = diff_list.index(lowest) + 4
print(index)

