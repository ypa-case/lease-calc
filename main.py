import logging
from datetime import datetime
import pandas as pd
from flask import Flask, render_template, flash, request, redirect

app = Flask(__name__)

def find_index(loon):
    #deze functie maakt een lijst van alle lonen uit de kolom tabelloon en zoekt vervolgens de bijhorende index
    #in het excel bestand
    df = pd.read_excel('wit_mnd_nl_std_alg_20210101.xlsx', engine='openpyxl')
    tabelloon_list = df['Tabelloon'].tolist()
    tabelloon_list.pop(-1)

    # om de juiste index te vinden voor de column in excel voor de ingevulde brutoloon
    diff_list = []
    for i in tabelloon_list:
        if type(i) == int:
            j = float(i)
            diff = loon - j
            diff_list.append(diff)
        elif type(i) == float:
            diff = loon - i
            diff_list.append(diff)

    lowest = diff_list[min(range(len(diff_list)), key=lambda i: abs(diff_list[i] - 0))]
    index = diff_list.index(lowest) + 4
    return index

def calculate(brutoloon, leasebudget, leasetarief, fiscalewaarde, bijtelling, extra_netto):
    df2 = pd.read_excel('wit_mnd_nl_std_alg_20210101.xlsx', engine='openpyxl', usecols="C")
    index = find_index(brutoloon)

    # inkomensbelasting zonder bijtelling
    netto_zonder_bijtelling = df2.iat[index - 2, 0]

    if leasetarief <= leasebudget:
        if bijtelling == "benzine":
            bijtelling_per_maand = (0.22 * fiscalewaarde) / 12
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand

            #find index of nieuw_bruto_loon
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto

            return netto_uitbetaald

        elif bijtelling == "elektrisch" and fiscalewaarde <= 40000:
            bijtelling_per_maand = (0.12 * fiscalewaarde) / 12
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto

            return netto_uitbetaald
        elif bijtelling == "elektrisch" and fiscalewaarde > 40000:
            bijtelling_per_maand = ((0.12 * 40000) + (0.22 * (fiscalewaarde - 40000))) / 12
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand

            # find index of nieuw_bruto_loon
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto

            return netto_uitbetaald
    elif leasetarief > leasebudget:
        if bijtelling == "benzine":
            bijtelling_per_maand = ((0.22 * fiscalewaarde) / 12) - (leasetarief - leasebudget)
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand

            # find index of nieuw_bruto_loon
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto - (leasetarief - leasebudget)

            return netto_uitbetaald

        elif bijtelling == "elektrisch" and fiscalewaarde <= 40000:
            bijtelling_per_maand = ((0.12 * fiscalewaarde) / 12) - (leasetarief - leasebudget)
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto - (leasetarief - leasebudget)

            return netto_uitbetaald
        elif bijtelling == "elektrisch" and fiscalewaarde > 40000:
            bijtelling_per_maand = (((0.12 * 40000) + (0.22 * (fiscalewaarde - 40000))) / 12) - (leasetarief - leasebudget)
            nieuw_bruto_loon = brutoloon + bijtelling_per_maand

            # find index of nieuw_bruto_loon
            index = find_index(nieuw_bruto_loon)

            # inkomensbelasting met bijtelling
            netto_met_bijtelling = df2.iat[index - 2, 0]

            netto_bijdrage = netto_met_bijtelling - netto_zonder_bijtelling
            netto_uitbetaald = brutoloon - netto_zonder_bijtelling - netto_bijdrage + extra_netto - (leasetarief - leasebudget)

            return netto_uitbetaald


@app.route('/', methods=['POST','GET'])
def index():
    if request.method == 'POST':
        brutoloon = int(request.form['brutoloon'])
        leasebudget = int(request.form['leasebudget'])
        leasetarief = int(request.form['leasetarief'])
        fiscalewaarde = int(request.form['fiscalewaarde'])
        bijtelling = request.form['bijtelling']

        extra_netto = int(request.form['netto_extra'])

        netto = calculate(brutoloon, leasebudget, leasetarief, fiscalewaarde, bijtelling, extra_netto)

        return render_template('index.html', netto=netto)
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)